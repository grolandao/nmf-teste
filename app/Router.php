<?php
namespace app;
/**
 * @method get(string $string, \Closure $param)
 * Todas as entradas de dados recebedas pelo metodo GET
 */
/**
 * @method post(string $string, \Closure $param)
 * Todas as entradas de dados recebidas pelo POST
 */
class Router
{
    private $request;
    private $supportedHttpMethods = array(
        "GET",
        "POST"
    );

    function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    function __call($name, $args)
    {
        list($route, $method) = $args;

        if(!in_array(strtoupper($name), $this->supportedHttpMethods))
        {
            $this->invalidMethodHandler();
        }

        $this->{strtolower($name)}[$this->formatRoute($route)] = $method;
    }

    private function formatRoute($route)
    {
        $result = rtrim($route, '/');
        if ($result === '')
        {
            return '/';
        }
        return $result;
    }

    private function invalidMethodHandler()
    {
        header("{$this->request->serverProtocol} 405 Method Not Allowed");
    }

    private function defaultRequestHandler()
    {
        header("{$this->request->serverProtocol} 404 Not Found");
    }

    function resolve()
    {
        $methodDictionary = (isset($this->{strtolower($this->request->requestMethod)}))? $this->{strtolower($this->request->requestMethod)}: null;
        $formatedRoute = $this->formatRoute($this->request->requestUri);

        $method = (isset($methodDictionary[$formatedRoute]))?$methodDictionary[$formatedRoute] : null;

        if(is_null($method))
        {
            $this->defaultRequestHandler();
            return;
        }

        echo call_user_func_array($method, array($this->request));
    }

    function __destruct()
    {
        $this->resolve();
    }

    public function on($method, $path, $callback)
    {
        $method = strtolower($method);
        if (!isset($this->routes[$method])) {
            $this->routes[$method] = [];
        }

        $uri = substr($path, 0, 1) !== '/' ? '/' . $path : $path;
        $pattern = str_replace('/', '\/', $uri);
        $route = '/^' . $pattern . '$/';

        $this->routes[$method][$route] = $callback;


        return $this;
    }

    function run($method, $uri)
    {
        $method = strtolower($method);
        if (!isset($this->routes[$method])) {
            return null;
        }
        $this->routes[$method];
        foreach ($this->routes[$method] as $route => $callback) {
            var_dump($route);
            var_dump($uri);
            echo '<br>';

            if (preg_match($route, $uri, $parameters)) {
                array_shift($parameters);
                var_dump($callback);
                return call_user_func_array($callback, $parameters);
            }
        }
        return null;
    }
}