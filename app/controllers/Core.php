<?php
namespace App\controllers;
class Core
{
    public $page;
    public function __construct()
    {

    }

    public function render()
    {
        var_dump(file_exists(__APP_ROOT__.'/assets/'.$this->page.'.html'));
        if(file_exists(__APP_ROOT__.'/assets/'.$this->page.'.html')){
            ob_start();
            include __APP_ROOT__.'/assets/'.$this->page.'.html';
            $output = ob_get_clean();

            echo $output;
        }
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }
}