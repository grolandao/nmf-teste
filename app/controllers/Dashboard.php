<?php
namespace App\controllers;
use App\controllers\Core;
class Dashboard extends Core
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){

        $this->setPage("dashboard");
        $this->render();
    }
}