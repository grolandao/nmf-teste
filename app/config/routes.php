<?php
use \App\controllers\Dashboard;
$router = new \App\Router(new \App\Request);


$router->get('/', function() {
    return <<<HTML
  <h1>Hello world</h1>
HTML;
});
$router->get('/teste', function($request) {
    $dashboard = new Dashboard();
    return $dashboard->index();
});
$router->post('/teste', function($request) {
    print_r($request->getBody());
});